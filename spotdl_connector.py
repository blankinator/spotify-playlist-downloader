import os


def download_playlist(playlist: dict) -> None:
    playlist_name = playlist["name"]
    output_root_folder = playlist["output_root_folder"]

    print(f"Downloading playlist {playlist_name}")

    # create playlist root, if not done yet
    try:
        os.mkdir(output_root_folder)
    except FileExistsError:
        pass
    os.chdir(output_root_folder)

    # create playlist folder if not don yet
    playlist_dir = os.path.join(output_root_folder, playlist_name)
    try:
        os.mkdir(playlist_dir)
        print(f"Playlist folder for {playlist_name} created")
    except FileExistsError:
        print(f"Playlist folder for {playlist_name} already created")

    os.chdir(playlist_dir)

    command = f"spotdl {playlist['external_urls']['spotify']} --format {playlist['file_type']}"
    os.system(command)
