# Spotify Playlist Downloader

## Requirements

- Global or local python environment with all packages from `requirements.txt`.
- FFMPEG according to the installation instructions for SpotDL. (https://ffmpeg.org/)

- Spotify-API client with ID and secret for SpotiPy. The following ENV variables have to be set:

```
SPOTIPY_CLIENT_ID='your_spotify_client_id'
SPOTIPY_CLIENT_SECRET='your_spotify_client_secret'
```

## Usage

The script has to be run from commandline with `python playlist_downloader.py <username> <playlist_root_dir>`
All public playlists by the given user will be downloaded.
If the playlist root does not exist yet, it will be created.
For each playlist a directory will be created in the playlist root.
The actual songs from the playlists will be downloaded to their respectively named directories.

A file called `additional_playlists.txt` can be used to specify additional playlists to download.
Each playlist has to be added as a new line containing the URI to the public playlist.