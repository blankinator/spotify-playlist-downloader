import sys
import os
import json

from spotdl_connector import download_playlist
from spotify_connection import get_playlists_by_user, get_playlist_info

CONFIG_FILE_PATH = "config.json"


def main() -> None:
    if os.path.isfile(CONFIG_FILE_PATH):
        with open(CONFIG_FILE_PATH, "r") as config_file:
            config = json.load(config_file)
    else:
        print("No config file (config.json) found. Exiting")
        sys.exit(1)

    # get default file type from config, set to mp3, if not given
    if "default_file_type" in config:
        default_file_type = config["default_file_type"]
    else:
        default_file_type = "mp3"

    # get root output folder from config
    if "output_root_folder" in config:
        output_root_folder = config["output_root_folder"]
    else:
        print("No output root folder specified in config. Exiting")
        sys.exit(1)

    # get users from which to fetch all public playlists
    if "users" in config:
        users = config["users"]
    else:
        users = list()

    # check for additional playlists
    if "additional_playlists" in config:
        additional_playlists = config["additional_playlists"]
    else:
        additional_playlists = list()

    # check for excluded playlists
    if "excluded_playlists" in config:
        excluded_playlists = config["excluded_playlists"]
    else:
        excluded_playlists = list()

    # check for specified playlist options
    if "playlist_options" in config:
        playlist_options = config["playlist_options"]
    else:
        playlist_options = dict()

    def get_playlist_meta(usernames: list, additional_playlists: list, excluded_playlists: list) -> list:
        # fetches the metadata for all the playlists to download
        user_playlists = [playlist for user in users for playlist in get_playlists_by_user(user)]
        additional_playlists = [get_playlist_info(add_playlist) for add_playlist in additional_playlists]
        all_playlists = [pl for pl in user_playlists + additional_playlists
                         if pl["external_urls"]["spotify"] not in excluded_playlists]
        return all_playlists

    playlist_meta = get_playlist_meta(users, additional_playlists, excluded_playlists)

    # create playlist dicts with options, use default values if no specific options are given
    default_playlist_options = {
        "file_type": default_file_type,
        "output_root_folder": output_root_folder
    }

    def get_playlist_with_options(playlist_meta: dict, playlist_options: dict, default_playlist_options: dict) -> dict:
        url = playlist_meta["external_urls"]["spotify"]
        # combine additional values with playlist meta
        return playlist_meta | default_playlist_options | (playlist_options[url] if url in playlist_options else {})

    playlist_dicts = [get_playlist_with_options(pl, playlist_options, default_playlist_options) for pl in playlist_meta]

    print(f"Downloading {len(playlist_dicts)} playlist(s)")
    print(f"Root for playlist folders: {output_root_folder}")
    [download_playlist(pl) for pl in playlist_dicts]

    print("Done.")


if __name__ == "__main__":
    main()
